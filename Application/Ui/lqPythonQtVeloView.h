// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifndef lqPythonQtVeloView_h
#define lqPythonQtVeloView_h


#include <PythonQt.h>
#include <QObject>

#include "lqVeloViewManager.h"
#include "Widgets/vvCalibrationDialog.h"
#include "Widgets/vvLaserSelectionDialog.h"
#include "Widgets/vvCropReturnsDialog.h"
#include "Widgets/vvSelectFramesDialog.h"

class lqPythonQtVeloView : public QObject
{
  Q_OBJECT

public:
  lqPythonQtVeloView(QObject* parent = 0)
    : QObject(parent)
  {
    this->registerClassForPythonQt(&lqVeloViewManager::staticMetaObject);

    this->registerClassForPythonQt(&vvCropReturnsDialog::staticMetaObject);
    this->registerClassForPythonQt(&vvSelectFramesDialog::staticMetaObject);

    this->registerClassForPythonQt(&vvCalibrationDialog::staticMetaObject);
    this->registerClassForPythonQt(&vvLaserSelectionDialog::staticMetaObject);
  }

  inline void registerClassForPythonQt(const QMetaObject* metaobject)
  {
    PythonQt::self()->registerClass(metaobject, "paraview");
  }

public Q_SLOTS:
  // Common with LV
  vvCropReturnsDialog* new_vvCropReturnsDialog(QWidget* arg0)
  {
    return new vvCropReturnsDialog(arg0);
  }

  vvSelectFramesDialog* new_vvSelectFramesDialog(QWidget* arg0)
  {
    return new vvSelectFramesDialog(arg0);
  }

  // vvLaserSelectionDialog
  vvCalibrationDialog* new_vvCalibrationDialog(QWidget* arg0)
  {
    return new vvCalibrationDialog(arg0);
  }

  vvLaserSelectionDialog* new_vvLaserSelectionDialog(QWidget* arg0)
  {
    return new vvLaserSelectionDialog(arg0);
  }

  QVector<int> getLaserSelectionSelector(vvLaserSelectionDialog* inst)
  {
    return inst->getLaserSelectionSelector();
  }

  void setLaserSelectionSelector(vvLaserSelectionDialog* inst, const QVector<int>& arg0)
  {
    inst->setLaserSelectionSelector(arg0);
  }

  bool isDisplayMoreSelectionsChecked(vvLaserSelectionDialog* inst)
  {
    return inst->isDisplayMoreSelectionsChecked();
  }

  void setDisplayMoreSelectionsChecked(vvLaserSelectionDialog* inst, bool arg0)
  {
    inst->setDisplayMoreSelectionsChecked(arg0);
  }

  void setLasersCorrections(vvLaserSelectionDialog* inst, const QVector<double>& arg0,
    const QVector<double>& arg1, const QVector<double>& arg2, const QVector<double>& arg3,
    const QVector<double>& arg4, const QVector<double>& arg5, const QVector<double>& arg6,
    const QVector<double>& arg7, const QVector<double>& arg8, const QVector<double>& arg9,
    const QVector<double>& arg10, int arg11)
  {
    inst->setLasersCorrections(
      arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11);
  }

};

#endif //lqPythonQtVeloView_h
