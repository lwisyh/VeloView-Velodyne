# Bundling Scripts Stack Entry for $lidarview_appname - Apple Specific
include(lidarview.bundle.common)

# Trigger Apple-specific VeloView Bundling
include(${LidarViewSuperBuild_CMAKE_DIR}/bundle/apple/LidarviewBundle.cmake)

# VeloView-Apple Specifics

# Install Sensor calibration files
file(GLOB shared_files "${share_path}/*.xml")
install(FILES ${shared_files}
        DESTINATION "${lidarview_appname}/Contents/Resources"
        COMPONENT superbuild)
unset(shared_files)


#Install Veloview User Guide
install(FILES "${superbuild_install_location}/${LV_INSTALL_RESOURCE_DIR}/VeloView_User_Guide.pdf"
  DESTINATION "${lidarview_appname}/Contents/Resources"
  COMPONENT superbuild
)
